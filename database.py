import psycopg2


class DataBase():
	"""docstring for DataBase"""
	def __init__(self, ip):
		self.ip = ip
		self.db_connection = psycopg2.connect(host=self.ip,
										dbname='sinapsis',
										user='pi',
										password='Password123'
										)
		self.db_connection.autocommit = True
		self.db_cursor = self.db_connection.cursor()

	def last_storaged_messages(self):
		self.db_cursor.execute("""SELECT * FROM SINAPSIS.MESSAGE ORDER BY MESSAGE.ID DESC LIMIT 2""")
		query = []
		for row in self.db_cursor.fetchall():
		    q = {}
		    c = 0
		    for col in self.db_cursor.description:
		        q.update({str(col[0]): row[c]})
		        c+=1
		    query.append(q)

		return query
		

		