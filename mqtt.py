import os 
import json 
import base64 
import time

import paho.mqtt.subscribe as subscribe 
from cryptography.fernet import Fernet 
import paho.mqtt.client as mqttc 

key = "UOC-DMXptS_t7JPi_omDofSfTYaYVTqn638NLMl_zYE=" 
cipher_suite = Fernet(key) 
count = 0

def on_connect(client, userdata, flags, rc):
	global all_messages_received
	print('connected (%s)' % client._client_id)
	all_messages_received = []



def on_message(client, userdata, message):
	msg = cipher_suite.decrypt(message.payload)
	msg = json.loads(msg)
	all_messages_received.insert(0,msg)

def get_device_sinapsis(topico_device, id_sinapsis, all=None, WAIT=5):
	"""Se recibe el topico que quiero escuchar y el id del sinapsis al cual se conectO la app.
	Tambien se podria recibir el parametro 'all':
	Si se recibe 'all' se envian todos los mensajes del sinapsis correspondiente al ID recibido durante WAIT segundos.
	Si no se recibe 'all' Se retorna el ultimo mensaje que llego durante WAIT segundos.

	Por ultimo se recibe el parametro WAIT, por default es 5 seg
	"""
	client = mqttc.Client()

	client.on_message = on_message
	client.on_connect = on_connect

	client.connect('vikua.com')
	client.loop_start() 
	client.subscribe(topico_device)
	time.sleep(WAIT) # wait
	client.disconnect()
	client.loop_stop() #stop the loop


	if all:
		msg_to_ret = []
		for diccionario in all_messages_received:
			for key, value in diccionario.items():
				if key == 'sinapsis':
					if value['id'] == str(id_sinapsis):
						msg_to_ret.append(diccionario)
		return msg_to_ret
	else:
		for diccionario in all_messages_received:
			for key, value in diccionario.items():
				if key == 'sinapsis':
					if value['id'] == str(id_sinapsis):
						try:
							return diccionario
						except Exception as e:
							print(e)
							return None
				

	print("No se recibio nada del sinapsis")
	return None



def get_status_sinapsis(topico_status, id_sinapsis, all=None, WAIT=5):
	"""Se recibe el topico de STATUS y el id del sinapsis al cual se conectO la app.
	Tambien se podria recibir el parametro 'all':
	Si se recibe 'all' se envian todos los mensajes del sinapsis correspondiente al ID recibido durante WAIT segundos.
	Si no se recibe 'all' Se retorna el ultimo mensaje que llego durante WAIT segundos.

	Por ultimo se recibe el parametro WAIT, por default es 5 seg
	"""
	client = mqttc.Client()

	client.on_message = on_message
	client.on_connect = on_connect

	client.connect('vikua.com')
	client.loop_start() 
	client.subscribe(topico_status)
	time.sleep(WAIT) # wait
	client.disconnect()
	client.loop_stop() #stop the loop


	if all_messages_received:
		
		if all:
			msg_to_ret = []
			try:
				for diccionario in all_messages_received:
					for key, value in diccionario.items():
						if key=='id' and value==str(id_sinapsis):
							msg_to_ret.append(diccionario)
				return msg_to_ret
			except Exception as e:
				return msg_to_ret
		else:
			for diccionario in all_messages_received:
				for key, value in diccionario.items():
					if key=='id' and value==str(id_sinapsis):
						return diccionario
	else:
		return all_messages_received #ret Vacio