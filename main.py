﻿
import os, ast
import imghdr
import weakref
import subprocess
import threading
from datetime import datetime, timedelta
import requests, time

from io import BytesIO
from PIL import Image as ImagePIL
from win32api import GetSystemMetrics
import kivy
kivy.require('1.9.2')
from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.modalview import ModalView
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.gridlayout import GridLayout
from kivy.clock import Clock
from kivy.uix.textinput import TextInput


from kivy.uix.image import AsyncImage, Image
from kivy.loader import Loader

from kivy.properties import ObjectProperty, ListProperty, StringProperty, BooleanProperty
from kivy.config import Config

from requests.auth import HTTPDigestAuth

import pysftp

from database import DataBase
from mqtt import *

import paramiko

WINDOW_WIDTH = GetSystemMetrics(0)
WINDOW_HEIGHT = GetSystemMetrics(1)

Config.set('graphics', 'width', int(WINDOW_WIDTH*.45))
Config.set('graphics', 'height', int(WINDOW_HEIGHT*.9))
Config.set('graphics', 'resizable', False)
Config.set('input', 'mouse', 'mouse,multitouch_on_demand') #Para desactivar

Config.set('kivy','window_icon','Images/SinapsisIcon.ico')

FONTSIZE_PRIMARY = int(round(WINDOW_HEIGHT*0.03))
FONTSIZE_SECONDARY = int(round(WINDOW_HEIGHT*0.025))
FONTSIZE_TERTIARY  = int(round(WINDOW_HEIGHT*0.019))
FONTSIZE_QUATERNARY = int(round(WINDOW_HEIGHT*0.016))

DEBUG = False

if DEBUG:
    IPS_CAMERAS = ['172.16.64.159', '172.16.64.149', '172.16.64.160', '172.16.64.129']
    
else:
    IPS_CAMERAS = ['192.168.1.81','192.168.1.82','192.168.1.83','192.168.1.84']
    

API_FECHA_CAMERA = "http://{}/cgi-bin/global.cgi?action=getCurrentTime" #USE METHOD: api_fecha_camara.format(X)

SINAPSIS_USER = 'pi'
SINAPSIS_PASS = 'Password123'

USER = 'admin'
PASSWORD = 'Password123'
CONF_PATH = '/home/pi/conf.cfg'

TOPICO_DEVICE = 'vikua/jtep/sinapsis/b/device'
TOPICO_STATUS = 'vikua/jtep/sinapsis/b/status'

SINAPSIS_USER = 'pi'
SINAPSIS_PASS = 'Password123'


Clock.max_iteration = 20


def pitch_pop(tit="Cargando",msj="Espere mientras se procesa la informacion"):
	pop = Popup(title=tit,content=Label(text=msj),
						auto_dismiss=False, size_hint=(1, None), height=150)
	pop.open()
	return pop



class WidgetApp(ScreenManager):
	back_color = ListProperty([0.01,0.33,0.01])

	def __init__(self, **kwarg):
		super(WidgetApp, self).__init__()

		try: #Limpiar todo lo q haya en /tmp
			print('Eliminando archivos temporales dentro de /tmp...')
			self.clean_tmp()
		except Exception as e:
			print("No hay archivos en /tmp ::::: ", e)



		self.startwidget = StartWidget(self) #instancia: StartWidget (pantalla principal) y paso por parametro la clase WidgetApp ("self")
		wid = Screen(name="start") #Instanciando una Pantalla
		wid.add_widget(self.startwidget) #Agg al Screen el widget StartWidget
		self.add_widget(wid) #Agg al WidgerApp


	def clean_tmp(self):
		for filename in os.listdir('tmp'):
			img_to_remove_path=".\tmp\{}".format(filename)
			os.remove(img_to_remove_path)
			print(img_to_remove_path,"<<<<<<<<<<<<< ELIMINADO")



	def goto_start(self):
		self.current = "start"
		

	def goto_home_assistant(self):
		self.current = "home"





class StartWidget(BoxLayout): # Widget de Pantalla Principal a la App
    textlabel = StringProperty("""[color=FFFFFF]Para el uso correcto de esta aplicacion debe conectarse a la Red Wifi del Sinapsis que desea gestionar.\nVersion 0.1[/color]""")
    colorbutton = ListProperty([0,0,1,1])
    textbutton = StringProperty("""Continuar""")


    def __init__(self, widgetapp, **kwargs): #Recibo por parametro widgetapp class y ya puedo acceder a sus metodos y atributos
        super(StartWidget, self).__init__()
        self.widgetapp = widgetapp  #Puedo acceder a los metodos y attr de WidgerApp

    def check_network(self):
        """Method called from .kv file.
        When the button is released:
        First: A popup is launched to obtain the Sinapsis IP (ip_validation function).
        Second: ping function is executed, that does the following:
            2.1. Ping to sinapsis network
            2.2. Wait a Ping response
            2.3. Closes the popup created in method execution
            2.4. Executes a function that renders to home if it is Success, else Error Screen
        Third: Launches a wait popup and make an action based on the ping response (action_to_execute function)  
        """
        
        
        self.ippopup = Popup(title="Ingrese la IP del Sinapsis",
                    auto_dismiss=False,
                    size_hint=(None, None),
                    size=(400, 200))

        self.boxlayout = BoxLayout(orientation='vertical')
        
        self.input = TextInput(focus=False, hint_text ='IP Sinapsis', multiline=False)
        self.button = Button(text='Aceptar', on_press=self.ip_validation)


        self.boxlayout.add_widget(self.input)
        self.boxlayout.add_widget(self.button)

        self.ippopup.add_widget(self.boxlayout)

        self.ippopup.open()
        
        
    def ip_validation(self, btn):
        """When the IP popup button is released, this function will be executed.
        Gets the input text and lauchs the Thread"""
        self.ip_sinapsis = self.input.text
        self.ippopup.dismiss()
        self.pop = pitch_pop(tit='Conectando con WiFi del Sinapsis. IP: {}'.format(self.ip_sinapsis),
            msj='Se esta intentando establecer conexion con la red WiFi del Sinapsis\nPor favor espere...')
        self.clock = Clock.schedule_interval(self.ping, 3)
        
        
    def ping(self, dt):
        """Execute Sinapsis Network Ping and wait a response, After, closes 
        the popup and executes an action"""
        
        self.ping_response = None
        # self.ping_response = os.system("ping -c 1 {} >/dev/null 2>&1".format(SINAPSIS_IP))
        try: #intenta hacer el ping
            self.ping_response = os.system("ping {} -n 1".format(self.ip_sinapsis))
        except Exception as e: #si manda error, asignar valor <> 0 a ping_response
            print(e)
            self.ping_response = 255
            self.pop.dismiss()
            self.clock.cancel()
            self.action_to_execute()



        while self.ping_response==None:
            time.sleep(0.5)
            pass

        self.pop.dismiss()
        self.clock.cancel()
        self.action_to_execute()




    def action_to_execute(self):
        """If: the Ping Response is Success, go to HOME
        else: the screen is modified to a Error Screen
        """
        if self.ping_response != 0:

            self.textlabel = """[color=AA6565]La aplicacion no ha podido conectarse a la Red Wifi del Sinapsis.\nAsegurese de estar conectado y vuelva a intentar[/color]"""

            self.colorbutton[0] = 1
            self.colorbutton[1] = 0
            self.colorbutton[2] = 0
            self.colorbutton[3] = 1

            self.textbutton = "Reintentar"
            print("Fail")

        else:
            print('Connected')
            self.HomeAssistantWidget = HomeAssistantWidget(self) #instancia: Home de la App y paso por parametro la clase WidgetApp ("self")
            wid = Screen(name="home") #Instanciando una Pantalla
            wid.add_widget(self.HomeAssistantWidget) #Agg al Screen el widget HomeAssistantWidget
            self.widgetapp.add_widget(wid) #Agg al WidgerApp
            self.widgetapp.goto_home_assistant()





class HomeAssistantWidget(BoxLayout):
    def __init__(self,  initial_view):
        super(HomeAssistantWidget, self).__init__()

        self.initial_view = initial_view
        self.ip_sinapsis = self.initial_view.ip_sinapsis
        
        self.id_sinapsis=None
        self.database = DataBase(self.ip_sinapsis)	
        self.original_height = (self.ids['labelinformationarea'].height * 10) - 10 #posicion original del labelinformationarea (altura-padding)


        #Last Messages
        wid = ViewLastSavedMsgButton(self)
        self.ids.gridleftmenu.add_widget(wid)

        wid = DetailLastSavedMsgButton(self)
        self.ids.gridleftmenu.add_widget(wid)


        #Check Datetime in Cameras
        wid = ViewCheckDatetimeCameraButton(self)
        self.ids.gridleftmenu.add_widget(wid)

        wid = DetailCheckDatetimeCameraButton(self)
        self.ids.gridleftmenu.add_widget(wid)


        #Take pics
        wid = ViewCameraPicture(self)
        self.ids.gridleftmenu.add_widget(wid)

        wid = DetailCameraPicture(self)
        self.ids.gridleftmenu.add_widget(wid)


        #Internet
        wid = ViewInternetConnection(self)
        self.ids.gridleftmenu.add_widget(wid)

        wid = DetailInternetConnection(self)
        self.ids.gridleftmenu.add_widget(wid)


        #Status sw
        wid = ViewCarSwitch(self)
        self.ids.gridleftmenu.add_widget(wid)

        wid = DetailCarSwitch(self)
        self.ids.gridleftmenu.add_widget(wid)


        #gps y temperatura
        wid = ViewGPSandTemperature(self)
        self.ids.gridleftmenu.add_widget(wid)

        wid = DetailGPSandTemperature(self)
        self.ids.gridleftmenu.add_widget(wid)


        #Listen topic
        wid = ViewListenSinapsisTopic(self)
        self.ids.gridleftmenu.add_widget(wid)

        wid = DetailListenSinapsisTopic(self)
        self.ids.gridleftmenu.add_widget(wid)


   
                        
                        
        #Se procede a conectarse por SSH al Sinapsis para leer el archivo .conf y extraer el ID del sinapsis
        try:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(self.ip_sinapsis, username='pi', password='Password123')
            (entrada2, salida2, error2) = ssh.exec_command('cat /etc/sinapsis/conf.cfg')
            for i in salida2.readlines():
                if '[sinapsis]' in i:
                    c1 = True
                if c1:
                    if 'id' in i:
                        var=i.split('=')
                        var=var[-1].rstrip("\n")
                        var=int(var)
                        self.id_sinapsis = var
                        print(self.id_sinapsis)
                        break

            ssh.close()
        except paramiko.AuthenticationException:
            print('pass invalid')
        except Exception as e:
            print(e)
            
            
        initial_text = """[color=0F0F0F][size=30][b]BIENVENIDO[/b][/size][size=20]\n\n\n\n\n[color={1}]ID Sinapsis: {0}[/color][/size][/color]"""

        if self.id_sinapsis:
            self.ids.labelinformationarea.text=initial_text.format(self.id_sinapsis, '1A5622')
        else:
            self.ids.labelinformationarea.text=initial_text.format('No se pudo obtener la informacion', '922528')





class ViewLastSavedMsgButton(Button):
    """docstring for ViewLastSavedMsgButton"""
    def __init__(self, padre_home):
        super(ViewLastSavedMsgButton, self).__init__()
        self.padre_home = padre_home #Puedo acceder a los metodos y attr de HomeAssistantWidget
        self.database = self.padre_home.database
        self.original_height = self.padre_home.original_height

    def last_saved_quickly(self):
        if self.padre_home.ids['labelinformationarea'].height != self.original_height: #si no tiene la posicion correcta
            self.padre_home.ids['gridimages'].height = 0 #oculto la grilla de imagenes
            self.padre_home.ids['labelinformationarea'].height = self.original_height #devuelvo tamano original

        try:
            lasts_msg = self.database.last_storaged_messages()[0]
            date = lasts_msg["date"].date()
            hour = lasts_msg["date"].strftime("%I:%M:%S %p")

            info = '[size={}]El ultimo mensaje almacenado fue el [b]{} a las {}[/b][/size]'.format(FONTSIZE_PRIMARY,date, hour)
            info = '[color=0F0F0F]{}[/color]'.format(info)
        except Exception as e:
            print(e)
            info = '[color=0F0F0F]Informacion no disponible[/color]'

        self.padre_home.ids['labelinformationarea'].text=info



class DetailLastSavedMsgButton(Button):
    def __init__(self, padre_home):
        super(DetailLastSavedMsgButton, self).__init__()
        self.padre_home = padre_home #Puedo acceder a los metodos y attr de HomeAssistantWidget
        self.database = self.padre_home.database
        self.original_height = self.padre_home.original_height

    def format_show_infarea(self):
        lasts_msg = self.database.last_storaged_messages()

        info = """[b]ULTIMO MENSAJE[/b]\n\n"""
        for dictionary in lasts_msg:
            for key, val in dictionary.items():
                info = info + "         [size={0}][b]{1}:[/b]  {2}[/size]\n".format(FONTSIZE_QUATERNARY,key,val)
            if dictionary != lasts_msg[-1] : info = info + "\n\n[b]PENULTIMO MENSAJE[/b]\n\n"

        info = "[color=0F0F0F][size={}]{}[/size][/color]".format(FONTSIZE_TERTIARY,info)
        self.padre_home.ids['labelinformationarea'].text=info
		

    def last_saved_query(self):
        if self.padre_home.ids['labelinformationarea'].height != self.original_height: #si no tiene la posicion correcta
            self.padre_home.ids['gridimages'].height = 0 #oculto la grilla de imagenes
            self.padre_home.ids['labelinformationarea'].height = self.original_height #devuelvo tamano original

        self.format_show_infarea()



class ViewCheckDatetimeCameraButton(Button):
    def __init__(self, padre_home):
        super(ViewCheckDatetimeCameraButton, self).__init__()
        self.padre_home = padre_home #Puedo acceder a los metodos y attr de HomeAssistantWidget
        self.original_height = self.padre_home.original_height #Puedo acceder a los metodos y attr de HomeAssistantWidget

    def delta_times_cameras(self):
        status_cams = {}
        text_area = "[size={}][b]ESTATUS DE FECHA/HORA EN CAMARAS:[/b][/size]\n\n\n".format(FONTSIZE_SECONDARY)
        for ip in IPS_CAMERAS:
            try:
                request_fecha_camara = requests.get(API_FECHA_CAMERA.format(ip), 
                                                    auth=(USER, PASSWORD), timeout=4)

                if request_fecha_camara.status_code > 250:
                    request_fecha_camara = requests.get(API_FECHA_CAMERA.format(ip), 
                                                        auth=HTTPDigestAuth(USER, PASSWORD), timeout=4)
                from_this = request_fecha_camara.text.find('=') #cut string from '=' to final date ---> '2019-04-16 15:19:25'
                datetime_camera = request_fecha_camara.text[from_this+1::]
                datetime_camera = datetime.strptime(datetime_camera, "%Y-%m-%d %H:%M:%S")
                time.sleep(1.5)
                delta_time = (datetime.now() - datetime_camera).seconds
                delta_time = int(delta_time/60) #guardando delta time en minutos
                if delta_time <=5:
                    text_area = text_area + "[size={}][b]Cam {}[/b] > [color=1A5622]Bien[/color][/size]\n\n".format(FONTSIZE_TERTIARY,ip)
                else:
                    text_area = text_area + "[size={}][b]Cam {}[/b] > [color=922528]Hora/Fecha desconfigurada[/color][/size]\n\n".format(FONTSIZE_TERTIARY,ip)


            except Exception as e:
                print(e)
                text_area = text_area + "[size={}][b]Cam {}[/b] > [color=922528]Error de solicitud[/color][/size]\n\n".format(FONTSIZE_TERTIARY,ip)
    
        text_area = "[color=0F0F0F][size={}]{}[/size][/color]".format(FONTSIZE_PRIMARY,text_area)
        self.padre_home.ids['labelinformationarea'].text = text_area
        self.pop.dismiss()


    def check_datetime_quickly(self):
        if self.padre_home.ids['labelinformationarea'].height != self.original_height: #si no tiene la posicion correcta
            self.padre_home.ids['gridimages'].height = 0 #oculto la grilla de imagenes
            self.padre_home.ids['labelinformationarea'].height = self.original_height #devuelvo tamano original

        thread = threading.Thread(target=self.delta_times_cameras)
        thread.start()

        self.pop=pitch_pop()
				


class DetailCheckDatetimeCameraButton(Button):
	"""docstring for DetailCheckDatetimeCameraButton"""
	def __init__(self, padre_home):
		super(DetailCheckDatetimeCameraButton, self).__init__()
		self.padre_home = padre_home #Puedo acceder a los metodos y attr de HomeAssistantWidget
		self.original_height = self.padre_home.original_height #Puedo acceder a los metodos y attr de HomeAssistantWidget

	def get_camera_datetime(self):
		text_area = "[size={}][b]DETALLE FECHA Y HORA DE CAMARAS[/b][/size]\n\n".format(FONTSIZE_SECONDARY)
		for ip in IPS_CAMERAS:
			try:
				request_fecha_camara = requests.get(API_FECHA_CAMERA.format(ip), 
													auth=(USER, PASSWORD), timeout=4)
				
				if request_fecha_camara.status_code > 250:
					request_fecha_camara = requests.get(API_FECHA_CAMERA.format(ip), 
														auth=HTTPDigestAuth(USER, PASSWORD), timeout=4)
				from_this = request_fecha_camara.text.find('=') #cut string from '=' to final date ---> '2019-04-16 15:19:25'
				datetime_camera = request_fecha_camara.text[from_this+1::]
				text_area = text_area + "[size={0}][b]Camara{1}:[/b]\n{2}[/size]\n\n\n".format(FONTSIZE_SECONDARY,ip, datetime_camera)

			except Exception as e:
				text_area = text_area + "[size={0}][b]Camara{1}:[/b]\n{2}[/size]\n\n\n".format(FONTSIZE_SECONDARY,ip, "No Disponible")

		text_area = "[size={}][color=0F0F0F]{}[/color][/size]".format(FONTSIZE_PRIMARY,text_area)
		self.padre_home.ids['labelinformationarea'].text = text_area
		self.pop.dismiss()


	def check_datetime(self):
		"""Method called from .kv file.
		When the button is released, First: a get_camera_datetime thread is executed that does the following:
			1. GET datetime of cameras from API and builds a Info string
			2. Sets info in text area
			3. Closes the popup created in method execution
		Second: Launch a wait popup
		"""
		if self.padre_home.ids['labelinformationarea'].height != self.original_height: #si no tiene la posicion correcta
			self.padre_home.ids['gridimages'].height = 0 #oculto la grilla de imagenes
			self.padre_home.ids['labelinformationarea'].height = self.original_height #devuelvo tamano original

		thread = threading.Thread(target=self.get_camera_datetime)
		thread.start()

		self.pop=pitch_pop()
		


class ViewInternetConnection(Button):
	"""docstring for ViewInternetConnection"""
	def __init__(self, padre_home):
		super(ViewInternetConnection, self).__init__()
		self.padre_home = padre_home #Puedo acceder a los metodos y attr de HomeAssistantWidget
		self.original_height = self.padre_home.original_height #Puedo acceder a los metodos y attr de HomeAssistantWidget

	def google_ping(self):
		ping_response = None
		ping_response = os.system("ping www.google.com -n 1")

		while ping_response==None:
			pass
		self.pop.dismiss()

		if ping_response == 0:
			text_area = '[size=30][color=1A5622]CONECTADO[/color][/size]'
		else:
			text_area = '[size=30][color=922528]SIN CONEXION[/color][/size]'
		text_area = '[size=40][b]INTERNET[/b][/size]\n\n\n{}'.format(text_area)
		text_area = "[color=0F0F0F]{}[/color]".format(text_area)
		self.padre_home.ids['labelinformationarea'].text = text_area
		self.pop.dismiss()


	def internet_quickly(self):
		"""Method called from .kv file.
		When the button is released, First: a Google Ping thread is executed that does the following:
			1. Ping to google.com
			2. Sets info in text area
			3. Closes the popup created in method execution
		Second: Launch a wait popup
		"""
		if self.padre_home.ids['labelinformationarea'].height != self.original_height: #si no tiene la posicion correcta
			self.padre_home.ids['gridimages'].height = 0 #oculto la grilla de imagenes
			self.padre_home.ids['labelinformationarea'].height = self.original_height #devuelvo tamano original

		thread = threading.Thread(target=self.google_ping)
		thread.start()
		self.pop = pitch_pop()



class DetailInternetConnection(Button):
	"""docstring for DetailInternetConnection"""
	def __init__(self, padre_home):
		super(DetailInternetConnection, self).__init__()
		self.padre_home = padre_home #Puedo acceder a los metodos y attr de HomeAssistantWidget
		self.original_height = self.padre_home.original_height

	def google_ping_detail(self):
		text_area = '[size=40][b]DETALLES DEL PING[/b][/size]\n\n\n'
		output = (subprocess.check_output("ping www.google.com -n 1".split()).decode("utf-8")).split('\n')
		for i in output:
			text_area = '{}\n \n{}'.format(text_area, i)
		
		info = '[color=0F0F0F]{}[/color]'.format(text_area)
		self.pop.dismiss()
		self.padre_home.ids['labelinformationarea'].text = info


	def internet_detail(self):
		"""Method called from .kv file.
		When the button is released, First: a Google Ping thread is executed that does the following:
			1. Ping of 4 pack to google.com
			2. Sets info in text area
			3. Closes the popup created in method execution
		Second: Launch a wait popup
		"""
		if self.padre_home.ids['labelinformationarea'].height != self.original_height: #si no tiene la posicion correcta
			self.padre_home.ids['gridimages'].height = 0 #oculto la grilla de imagenes
			self.padre_home.ids['labelinformationarea'].height = self.original_height #devuelvo tamano original
		thread = threading.Thread(target=self.google_ping_detail)
		thread.start()
		self.pop = pitch_pop()



class ViewCameraPicture(Button):
    """docstring for ViewCameraPicture"""
    def __init__(self, padre_home):
        super(ViewCameraPicture, self).__init__()
        self.padre_home = padre_home #Puedo acceder a los metodos y attr de HomeAssistantWidget
        self.original_height = self.padre_home.original_height


    def clean_tmp(self):
        folder_temp = 'tmp'
        for filename in os.listdir(folder_temp):
            img_to_remove_path="{}/{}".format(folder_temp,filename)
            os.remove(img_to_remove_path)
            print(img_to_remove_path,"<<<<<<<<<<<<< ELIMINADO")


    def pictures(self):
        self.padre_home.ids['gridimages'].clear_widgets()
        try:
            self.clean_tmp()
        except Exception as e:
            print("No hay tmps", e)

        grid_images = self.padre_home.ids['gridimages']
        for url in IPS_CAMERAS:
            try:
                path = 'http://{}/cgi-bin/snapshot.cgi'.format(url)
                image = requests.get(path, auth=(USER, PASSWORD))
                if image.status_code > 250:
                    image = requests.get(path,auth=HTTPDigestAuth(USER, PASSWORD))
                    if image.status_code > 250:
                        path = 'http://{}/image?res=full&x0=0&y0=0&x1=2048&y1=1536&quality=18&doublescan=0'\
                            .format(url)
                        image = requests.get(path)
   
                img = BytesIO(image.content)
                print(11)
                img = ImagePIL.open(img)
                print(1)
                img_name = 'tmp/{}at{}.jpg'.format(url,datetime.now().strftime('%Y_%m_%d_%H_%M_%S'))
                print(img_name,"<-------------Photo taked 2")
                img.save(img_name)
                print(3)
                print('https://stackoverflow.com/questions/23587426/pil-open-method-not-working-with-bytesio INTENTA CON ESTO')
                canvas = AsyncImage(source = 'Images/error2.jpg')
                grid_images.add_widget(canvas)


            except Exception as e:
                print("ERROR ::",e)
                canvas = AsyncImage(source = 'Images/error2.jpg')

                grid_images.add_widget(canvas)

        grid_images.height = self.original_height #le doy a GridImages la posicion Original del information area
        self.pop.dismiss()

	


    def camera_pic_quickly(self):
        Loader.loading_image = 'Images/loading.gif'

        thread = threading.Thread(target=self.pictures)
        thread.start()
        self.pop=pitch_pop(msj="Capturando fotografias, por favor espere...")

	

class DetailCameraPicture(Button):
	"""docstring for DetailCameraPicture"""
	def __init__(self, padre_home):
		super(DetailCameraPicture, self).__init__()
		self.padre_home = padre_home #Puedo acceder a los metodos y attr de HomeAssistantWidget
		self.original_height = self.padre_home.original_height

	def camera_pic_detail(self):
		if self.padre_home.ids['labelinformationarea'].height != self.original_height: #si no tiene la posicion correcta
			self.padre_home.ids['gridimages'].height = 0 #oculto la grilla de imagenes
			self.padre_home.ids['labelinformationarea'].height = self.original_height #devuelvo tamano original


		self.padre_home.ids['labelinformationarea'].text = "PROXIMAMENTE DETALLE DE LAS FOTOS"



class ViewCarSwitch(Button):
    """docstring for ViewCarSwitch"""
    def __init__(self, padre_home):
        super(ViewCarSwitch, self).__init__()
        self.padre_home = padre_home #Puedo acceder a los metodos y attr de HomeAssistantWidget
        self.original_height = self.padre_home.original_height

    def make_request(self, topico, id_sinapsis):
        """Este hilo ayuda a solicitar informacion al sinapsis, debe recibir por parametro el topico que
        se desea escuchar, el id del sinapsis, el parametro del JSON que se desea (No obligatorio)...
        Ver tambien ---> Docstring de mqtt.py - funcion get_sinapsis_response()"""
        try:
            sw = get_status_sinapsis(topico, id_sinapsis)
            if len(sw):
                
                if sw['variables']['sw']==True:
                    sw = '[color=1A5622]ENCENDIDO[/color]'
                else:
                    sw = '[color=922528]APAGADO[/color]'
            else:
                sw = '[color=0F0F0F]Informacion no disponible[/color]'
            
        except Exception as e:
            self.padre_home.ids['labelinformationarea'].text = "Error {}".format(e)
            
        text = "[size={}][color=0F0F0F][b]ESTADO DEL SWITCH DEL VEHICULO\n\n\n[/size][/b][size={}]Switch {}[/size][/color]".format(FONTSIZE_PRIMARY,FONTSIZE_SECONDARY,sw)

        self.padre_home.ids['labelinformationarea'].text = text
        self.pop.dismiss()


    def car_sw_quickly(self):
        if self.padre_home.ids['labelinformationarea'].height != self.original_height: #si no tiene la posicion correcta
            self.padre_home.ids['gridimages'].height = 0 #oculto la grilla de imagenes
            self.padre_home.ids['labelinformationarea'].height = self.original_height #devuelvo tamano original


        thread = threading.Thread(target=self.make_request,
                                args=(TOPICO_STATUS,
                                    self.padre_home.id_sinapsis)
                                    )
        thread.start()
        self.pop=pitch_pop()

		

class DetailCarSwitch(Button):
	"""docstring for DetailCarSwitch"""
	def __init__(self, padre_home):
		super(DetailCarSwitch, self).__init__()
		self.padre_home = padre_home #Puedo acceder a los metodos y attr de HomeAssistantWidget
		self.original_height = self.padre_home.original_height

	def car_sw_detail(self):
		if self.padre_home.ids['labelinformationarea'].height != self.original_height: #si no tiene la posicion correcta
			self.padre_home.ids['gridimages'].height = 0 #oculto la grilla de imagenes
			self.padre_home.ids['labelinformationarea'].height = self.original_height #devuelvo tamano original
		self.padre_home.ids['labelinformationarea'].text = "PROXIMAMENTE DETALLE DEL SW"



class ViewGPSandTemperature(Button):
	"""docstring for ViewGPSandTemperature"""
	def __init__(self, padre_home):
		super(ViewGPSandTemperature, self).__init__()
		self.padre_home = padre_home #Puedo acceder a los metodos y attr de HomeAssistantWidget
		self.original_height = self.padre_home.original_height


	def make_request(self, topico, id_sinapsis):
		"""Este hilo ayuda a solicitar informacion al sinapsis, debe recibir por parametro el topico que
		se desea escuchar, el id del sinapsis, el parametro del JSON que se desea (No obligatorio)...
		Ver tambien ---> Docstring de mqtt.py - funcion get_sinapsis_response()"""
		
		try:
			sinapsis_msg = get_status_sinapsis(topico_status=topico, id_sinapsis=id_sinapsis)
		except Exception as e:
			self.padre_home.ids['labelinformationarea'].text = "Error {}".format(e)


		if sinapsis_msg:
			gps = '[color=1A5622]Disponible[/color]' if sinapsis_msg['variables']['gps'] else '[color=922528]No disponible[/color]'
			temperature = sinapsis_msg['variables']['temperature']
			try:	
				if int(temperature) <= 50:
					temperature =  '[color=045380]{}°C[/color]'.format(temperature)
				elif int(temperature) <= 70:
					temperature =  '[color=A84E1A]{}°C[/color]'.format(temperature)
				else:
					temperature =  '[color=922528]{}°C[/color]'.format(temperature)

			except Exception as e:
				print(e)
				temperature = '[color=922528]Ha ocurrido un error\n[size=15]{}[/size][/color]'.format(e)
		
		else:
			gps = '[color=922528]No se ha recibido informacion del sinapsis\n[/color]'
			temperature = '[color=922528]No se ha recibido informacion del sinapsis\n[/color]'

		



		text = "[color=0F0F0F][size=30][b]GPS\n{}\n\n\nTEMPERATURA\n{}[/b][/size][/color]".format(gps, temperature)

		self.padre_home.ids['labelinformationarea'].text = text
		self.pop.dismiss()



	def gps_temperature_quickly(self):
		if self.padre_home.ids['labelinformationarea'].height != self.original_height: #si no tiene la posicion correcta
			self.padre_home.ids['gridimages'].height = 0 #oculto la grilla de imagenes
			self.padre_home.ids['labelinformationarea'].height = self.original_height #devuelvo tamano original
		
		thread = threading.Thread(target=self.make_request,
								args=(TOPICO_STATUS,
									self.padre_home.id_sinapsis)
								)
		thread.start()
		self.pop=pitch_pop()

				

class DetailGPSandTemperature(Button):
	"""docstring for DetailGPSandTemperature"""
	def __init__(self, padre_home):
		super(DetailGPSandTemperature, self).__init__()
		self.padre_home = padre_home #Puedo acceder a los metodos y attr de HomeAssistantWidget
		self.original_height = self.padre_home.original_height


	def gps_temperature_detail(self):
		if self.padre_home.ids['labelinformationarea'].height != self.original_height: #si no tiene la posicion correcta
			self.padre_home.ids['gridimages'].height = 0 #oculto la grilla de imagenes
			self.padre_home.ids['labelinformationarea'].height = self.original_height #devuelvo tamano original
		self.padre_home.ids['labelinformationarea'].text = "PROXIMAMENTE DETALLE DE GPS Y TEMPERATURA"



class ViewListenSinapsisTopic(Button):
	"""docstring for ViewListenSinapsisTopic"""
	def __init__(self, padre_home):
		super(ViewListenSinapsisTopic, self).__init__()
		self.padre_home = padre_home #Puedo acceder a los metodos y attr de HomeAssistantWidget
		self.original_height = self.padre_home.original_height

	def make_request(self, topico, id_sinapsis):
		self.padre_home.ids['labelinformationarea'].text = "[color=0F0F0F]MENSAJES DEL SINAPSIS\n\n\n[/color]"
		sinapsis_msg = get_device_sinapsis(topico, id_sinapsis, True, 15) #True para recibir todos los mensajes
		if sinapsis_msg:
			for msg in sinapsis_msg:
				self.padre_home.ids['labelinformationarea'].text+="[color=000000][size={}]{}[/size]\n\n[/color]".format(FONTSIZE_QUATERNARY-2,msg)
		else:
			self.padre_home.ids['labelinformationarea'].text = "No se recibieron mensajes"

		self.pop.dismiss()

	
	def listen_topic_quickly(self):
		if self.padre_home.ids['labelinformationarea'].height != self.original_height: #si no tiene la posicion correcta
			self.padre_home.ids['gridimages'].height = 0 #oculto la grilla de imagenes
			self.padre_home.ids['labelinformationarea'].height = self.original_height #devuelvo tamano original
		
		thread = threading.Thread(target=self.make_request,
								args=(TOPICO_DEVICE,
									self.padre_home.id_sinapsis)
								)
		thread.start()
		self.pop=pitch_pop(msj="Escuchando los mensajes en el topico")
		

class DetailListenSinapsisTopic(Button):
	"""docstring for DetailListenSinapsisTopic"""
	def __init__(self, padre_home):
		super(DetailListenSinapsisTopic, self).__init__()
		self.padre_home = padre_home #Puedo acceder a los metodos y attr de HomeAssistantWidget
		self.original_height = self.padre_home.original_height

	
	def listen_topic_detail(self):
		if self.padre_home.ids['labelinformationarea'].height != self.original_height: #si no tiene la posicion correcta
			self.padre_home.ids['gridimages'].height = 0 #oculto la grilla de imagenes
			self.padre_home.ids['labelinformationarea'].height = self.original_height #devuelvo tamano original
		self.padre_home.ids['labelinformationarea'].text = "PROXIMAMENTE DETALLE TOPICO"




class MainApp(App):
	title = 'Sinapsis Assistant'
	def open_settings(*args):
		pass


	def build(self):
		return WidgetApp()


if __name__ == "__main__":
	MainApp().run()
